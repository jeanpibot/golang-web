package main

import (
	"fmt"
	"net/http"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Whoa, Go is neat!")
}

func aboutHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1> Hello to about handler </h1>")
}

func main() {
	//the way to write handle func
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/about", aboutHandler)
	//This another way to write  handle func
	http.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<p> Esto es para probar la función dentro del handle func</p>")
	})
	http.ListenAndServe(":8000", nil)

}
