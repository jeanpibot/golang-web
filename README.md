# Golang Web

It is a simple project which has a goal to practice the Golang language and learn more about this technology.

# Run and Building

If you just want to run, you can use a command which is:

```
go run promedio.go
```

Then build it with the go tool:

```
go build promedio.go
```

The command above will build an executable named main in the current directory alongside your source code. Execute it to see extraction of data:

```
./promedio.go
```

__NOTE:__ Just is executable for UNIX system. For execute into Windows system click on the main.exe



# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


# Authors
* **Jean Pierre Giovanni Arenas Ortiz**

# License
[MIT](https://choosealicense.com/licenses/mit/)